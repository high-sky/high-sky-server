CREATE TABLE airport (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  city VARCHAR(255) NOT NULL
);
CREATE UNIQUE INDEX idx_airport_city_name ON airport(name, city);

CREATE TABLE route (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);
CREATE UNIQUE INDEX idx_route_name ON route(name);

CREATE TABLE track_item (
  id SERIAL PRIMARY KEY,
  source_airport_id INT NOT NULL
    CONSTRAINT fk_track_item_source_airport REFERENCES airport(id),
  dest_airport_id INT NOT NULL
    CONSTRAINT fk_track_item_dest_airport REFERENCES airport(id),
  route_id INT NOT NULL
    CONSTRAINT fk_track_item_route REFERENCES route(id),
  track_pos INT DEFAULT 1,
  dates hstore
);
CREATE INDEX idx_track_item_source_airport ON track_item(source_airport_id);
CREATE INDEX idx_track_item_dest_airport ON track_item(dest_airport_id);
CREATE INDEX idx_track_item_route ON track_item(route_id);