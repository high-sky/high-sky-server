INSERT INTO airport (city, name) VALUES ('Москва', 'Внуково');
INSERT INTO airport (city, name) VALUES ('Москва', 'Шереметьево');
INSERT INTO airport (city, name) VALUES ('Москва', 'Домодедово');
INSERT INTO airport (city, name) VALUES ('СПб', 'Пулково');
INSERT INTO airport (city, name) VALUES ('Симферополь', 'Симферополь');

INSERT INTO route (name) VALUES ('М1');
INSERT INTO route (name) VALUES ('М2');
INSERT INTO route (name) VALUES ('М3');

-- М1 - Москва (Шереметьево) - Санкт Петербург (Пулково) и обратно; два раза в день
INSERT INTO track_item (source_airport_id, dest_airport_id, route_id, track_pos, dates) VALUES (
  (SELECT id FROM airport WHERE name = 'Шереметьево'),
  (SELECT id FROM airport WHERE name = 'Пулково'),
  (SELECT id FROM route WHERE name = 'М1'),
  1,
  'MON=>2,TUE=>2,WEN=>2,THU=>2,FRI=>2,SAT=>2,SUN=>2'::hstore
);

INSERT INTO track_item (source_airport_id, dest_airport_id, route_id, track_pos, dates) VALUES (
  (SELECT id FROM airport WHERE name = 'Пулково'),
  (SELECT id FROM airport WHERE name = 'Шереметьево'),
  (SELECT id FROM route WHERE name = 'М1'),
  1,
  'MON=>2,TUE=>2,WEN=>2,THU=>2,FRI=>2,SAT=>2,SUN=>2'::hstore
);

-- М2 Москва (Внуково) - Симферополь и обратно; один раз в день
INSERT INTO track_item (source_airport_id, dest_airport_id, route_id, track_pos, dates) VALUES (
  (SELECT id FROM airport WHERE name = 'Внуково'),
  (SELECT id FROM airport WHERE name = 'Симферополь'),
  (SELECT id FROM route WHERE name = 'М2'),
  1,
  'MON=>1,TUE=>1,WEN=>1,THU=>1,FRI=>1,SAT=>1,SUN=>1'::hstore
);

INSERT INTO track_item (source_airport_id, dest_airport_id, route_id, track_pos, dates) VALUES (
  (SELECT id FROM airport WHERE name = 'Симферополь'),
  (SELECT id FROM airport WHERE name = 'Внуково'),
  (SELECT id FROM route WHERE name = 'М2'),
  1,
  'MON=>1,TUE=>1,WEN=>1,THU=>1,FRI=>1,SAT=>1,SUN=>1'::hstore
);

-- М3 Санкт-Петербург (Пулково) - Москва (Домодедово) - Симферополь и обратно;
-- раз в неделю по субботам (в Симферополь) и обратно (в Санкт-Петербург) по воскресеньям.
-- Туда
INSERT INTO track_item (source_airport_id, dest_airport_id, route_id, track_pos, dates) VALUES (
  (SELECT id FROM airport WHERE name = 'Пулково'),
  (SELECT id FROM airport WHERE name = 'Домодедово'),
  (SELECT id FROM route WHERE name = 'М3'),
  1,
  'SAT=>1'::hstore
);

INSERT INTO track_item (source_airport_id, dest_airport_id, route_id, track_pos, dates) VALUES (
  (SELECT id FROM airport WHERE name = 'Домодедово'),
  (SELECT id FROM airport WHERE name = 'Симферополь'),
  (SELECT id FROM route WHERE name = 'М3'),
  2,
  'SAT=>1'::hstore
);

-- Обратно
INSERT INTO track_item (source_airport_id, dest_airport_id, route_id, track_pos, dates) VALUES (
  (SELECT id FROM airport WHERE name = 'Симферополь'),
  (SELECT id FROM airport WHERE name = 'Домодедово'),
  (SELECT id FROM route WHERE name = 'М3'),
  1,
  'SUN=>1'::hstore
);

INSERT INTO track_item (source_airport_id, dest_airport_id, route_id, track_pos, dates) VALUES (
  (SELECT id FROM airport WHERE name = 'Домодедово'),
  (SELECT id FROM airport WHERE name = 'Пулково'),
  (SELECT id FROM route WHERE name = 'М3'),
  2,
  'SUN=>1'::hstore
);