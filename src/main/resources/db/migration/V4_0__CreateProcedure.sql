DROP FUNCTION IF EXISTS track_info(VARCHAR(255));

CREATE OR REPLACE FUNCTION track_info(airport_id INT)
  RETURNS TABLE (route VARCHAR(255), sum BIGINT) AS
$$
BEGIN
  RETURN QUERY
  SELECT
    (SELECT name FROM route r WHERE r.id = ti.route_id) as route,
    sum(t::INT) as sum
  FROM track_item ti JOIN svals(ti.dates) t ON true
  WHERE ti.dest_airport_id = airport_id
  GROUP BY ti.route_id;
END
$$
LANGUAGE plpgsql;