package ru.romanow.highsky.trackInfo.repository;

import ru.romanow.highsky.trackInfo.domain.TrackInfo;

import javax.annotation.Nonnull;
import java.util.List;

public interface TrackInfoRepositoryCustom {

    @Nonnull
    List<TrackInfo> findTrackInfo(@Nonnull Integer airport);
}
