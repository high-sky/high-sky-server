package ru.romanow.highsky.trackInfo.repository;

import org.springframework.data.repository.CrudRepository;
import ru.romanow.highsky.trackInfo.domain.TrackInfo;

public interface TrackInfoRepository
        extends CrudRepository<TrackInfo, Integer>,
                TrackInfoRepositoryCustom {}
