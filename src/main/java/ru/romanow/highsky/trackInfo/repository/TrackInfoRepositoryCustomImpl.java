package ru.romanow.highsky.trackInfo.repository;

import ru.romanow.highsky.trackInfo.domain.TrackInfo;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.List;

public class TrackInfoRepositoryCustomImpl
        implements TrackInfoRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public List<TrackInfo> findTrackInfo(@Nonnull Integer airport) {
        StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery("findTrackInfo");
        query.setParameter("airport_id", airport);
        if (!query.execute()) {
            throw new RuntimeException("Store procedure request failed");
        }
        return (List<TrackInfo>) query.getResultList();
    }
}
