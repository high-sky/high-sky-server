package ru.romanow.highsky.trackInfo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@Accessors(chain = true)
@Entity
@NamedStoredProcedureQuery(
        name = "findTrackInfo",
        procedureName = "track_info",
        parameters = @StoredProcedureParameter(name = "airport_id", type = Integer.class),
        resultSetMappings = "trackInfoMapping"
)
@SqlResultSetMapping(name = "trackInfoMapping", classes = {
        @ConstructorResult(targetClass = TrackInfo.class,
                           columns = {
                                   @ColumnResult(name = "route", type = String.class),
                                   @ColumnResult(name = "sum", type = Integer.class)
                           })
})
public class TrackInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String route;
    private Integer sum;

    public TrackInfo() {}

    public TrackInfo(String route, Integer sum) {
        this.route = route;
        this.sum = sum;
    }
}
