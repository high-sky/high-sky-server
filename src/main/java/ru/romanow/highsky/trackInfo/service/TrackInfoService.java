package ru.romanow.highsky.trackInfo.service;

import ru.romanow.highsky.airport.domain.TrackInfoResponse;

import javax.annotation.Nonnull;
import java.util.List;

public interface TrackInfoService {
    List<TrackInfoResponse> findTrackInfo(@Nonnull Integer airport);
}
