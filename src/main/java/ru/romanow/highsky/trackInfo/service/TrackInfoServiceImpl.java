package ru.romanow.highsky.trackInfo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.romanow.highsky.airport.domain.TrackInfoResponse;
import ru.romanow.highsky.trackInfo.repository.TrackInfoRepository;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TrackInfoServiceImpl
        implements TrackInfoService {
    private final TrackInfoRepository trackInfoRepository;

    @Override
    @Transactional(readOnly = true)
    public List<TrackInfoResponse> findTrackInfo(@Nonnull Integer airport) {
        return trackInfoRepository
                .findTrackInfo(airport)
                .stream()
                .map(t -> new TrackInfoResponse(t.getRoute(), t.getSum()))
                .collect(Collectors.toList());
    }
}
