package ru.romanow.highsky.airport.web;

import lombok.AllArgsConstructor;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.romanow.highsky.airport.domain.TrackInfoResponse;
import ru.romanow.highsky.airport.service.AirportService;
import ru.romanow.highsky.common.model.ItemInfo;
import ru.romanow.highsky.common.model.PublicData;
import ru.romanow.highsky.trackInfo.service.TrackInfoService;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
@AllArgsConstructor
public class AirportController {
    private final AirportService airportService;
    private final TrackInfoService trackInfoService;

    @GetMapping(value = "/airports", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PublicData> airports() {
        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.maxAge(1, TimeUnit.HOURS))
                .body(new PublicData(airportService.airports()));
    }

    @GetMapping(value = "/info", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<TrackInfoResponse> trackInfo(@RequestParam Integer airport) {
        return trackInfoService.findTrackInfo(airport);
    }
}
