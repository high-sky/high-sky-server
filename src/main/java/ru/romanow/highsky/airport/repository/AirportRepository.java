package ru.romanow.highsky.airport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.romanow.highsky.airport.domain.Airport;

public interface AirportRepository
        extends JpaRepository<Airport, Integer> {}
