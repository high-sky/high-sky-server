package ru.romanow.highsky.airport.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class TrackInfoResponse {
    private String route;
    private Integer sum;
}
