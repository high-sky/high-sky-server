package ru.romanow.highsky.airport.service;

import ru.romanow.highsky.airport.domain.TrackInfoResponse;
import ru.romanow.highsky.common.model.ItemInfo;

import javax.annotation.Nonnull;
import java.util.List;

public interface AirportService {
    List<ItemInfo> airports();
}
