package ru.romanow.highsky.airport.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.romanow.highsky.airport.domain.TrackInfoResponse;
import ru.romanow.highsky.airport.repository.AirportRepository;
import ru.romanow.highsky.common.model.ItemInfo;
import ru.romanow.highsky.trackInfo.repository.TrackInfoRepository;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AirportServiceImpl
        implements AirportService {
    private final AirportRepository airportRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ItemInfo> airports() {
        return airportRepository
                .findAll()
                .stream()
                .map(a -> new ItemInfo(a.getId(), a.getName()))
                .collect(Collectors.toList());
    }
}
